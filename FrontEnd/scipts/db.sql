-- Database: mejengApp

-- DROP DATABASE "mejengApp";

CREATE DATABASE "mejengApp"
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'English_United States.1252'
    LC_CTYPE = 'English_United States.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
	
	
CREATE TABLE player(
 user_id serial PRIMARY KEY,
 username VARCHAR (50) UNIQUE NOT NULL,
 password VARCHAR (50) NOT NULL,
 name VARCHAR(50) NOT NULL,
 role bit NOT NULL,
 email VARCHAR (355) UNIQUE NOT NULL,
 phone VARCHAR(50) NOT NULL
);

CREATE TABLE team(
 team_id serial PRIMARY KEY,
 name VARCHAR (50) UNIQUE NOT NULL,
 players_quantity INTEGER NOT NULL
);

CREATE TABLE player_team (
  player_id    int REFERENCES player (user_id) ON UPDATE CASCADE ON DELETE CASCADE
, team_id int REFERENCES team (team_id) ON UPDATE CASCADE ON DELETE CASCADE
, CONSTRAINT player_team_pkey PRIMARY KEY (player_id, team_id)  -- explicit pk
);

CREATE TABLE game(
 game_id serial PRIMARY KEY,
 score VARCHAR (50),
 scheduled TIME NOT NULL,
 field_id INTEGER NOT NULL,
	CONSTRAINT field_id_fkey FOREIGN KEY (field_id)
      REFERENCES game_field (field_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE game_teams(
 game_id    int REFERENCES game (game_id) ON UPDATE CASCADE ON DELETE NO ACTION
, team_id int REFERENCES team (team_id) ON UPDATE CASCADE ON DELETE NO ACTION
, CONSTRAINT game_teams_pkey PRIMARY KEY (game_id, team_id)  -- explicit pk
);

CREATE TABLE game_field(
 field_id serial PRIMARY KEY,
 name VARCHAR (100) UNIQUE NOT NULL,
 schedule VARCHAR (50) NOT NULL,
 capacity INTEGER NOT NULL,
 address VARCHAR (200) NOT NULL,
 phone VARCHAR(50) NOT NULL
);


INSERT INTO public.player(
	user_id, username, password, name, role, email, phone)
	VALUES (1, 'erortega', 'password', 'Esteban Ortega', B'1', 'eortega1793@gmail.com', '88884455');
	
INSERT INTO public.player(
	user_id, username, password, name, role, email, phone)
	VALUES (2, 'jmesen', 'password', 'Jose Mesen', B'1', 'mail@gmail.com', '88445588');



--DROP TABLE game;

--drop table game_teams;