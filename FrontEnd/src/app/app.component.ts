import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserDataService } from "./service/user-data.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  name:string;
  title = 'mejengapp';

  constructor(
    public router: Router,
    private userData: UserDataService
  ) { }

  ngOnInit() {
    this.userData.currentName.subscribe(name => this.name = name)
  }

  goHome() {
    this.router.navigate(['/dashboard'])
  }

  goToTeams() {
    this.router.navigate(['/teams'])
  }

  goToFields() {
    this.router.navigate(['/fields'])
  }

  goToGames() {
    this.router.navigate(['/games'])
  }

  goToLogin() {
    this.newName("")
    this.newId("")
    this.router.navigate(['/sign-in'])
  }

  newName(content) {
    this.userData.changeName(content);
  }

  newId(content) {
    this.userData.changeId(content);
  }
}
