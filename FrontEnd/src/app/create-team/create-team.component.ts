import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from "../service/rest-api.service";
import { UserDataService } from "../service/user-data.service";
import { Team } from '../service/team';

@Component({
  selector: 'app-create-team',
  templateUrl: './create-team.component.html',
  styleUrls: ['./create-team.component.css']
})
export class CreateTeamComponent implements OnInit {

  @Input() teamDetails = { team_id: '', name: '', players_quantity: 0}
  Teams: any = [];
  errorMessage = '';
  team: Team = new Team;

  constructor(public restApi: RestApiService, 
    public router: Router,
    private userData: UserDataService) { }

  ngOnInit() {
    this.restApi.getTeams().subscribe(
      (data: {}) => {
        this.Teams = data;   
      }
    )
  }

  registerTeam(){
    if(this.checkTeamData()){
      console.log("Creating team");
      
      this.team.name = this.teamDetails.name;
      this.team.players_quantity = 0;
      let date = new Date();
      this.team.createdAt = "2019-10-10T06:00:00.000Z";
      this.team.updatedAt = "2019-10-10T06:00:00.000Z";

      this.restApi.createTeam(this.team)
      this.router.navigate(['/teams'])
    }
  }

  checkTeamData(){
    if(this.teamDetails.name === ""){
      this.errorMessage = "Empty name field"
      return false
    }
    return true
  }

}
