import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserDataService } from "../service/user-data.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  name:string;
  user_id:string;

  constructor(
    public router: Router,
    private userData: UserDataService
  ) { }

  ngOnInit() {
    this.userData.currentName.subscribe(name => this.name = name)
    this.userData.currentUserId.subscribe(user_id => this.user_id = user_id)
  }

  goToGames() {
    this.router.navigate(['/games'])
  }

  goToTeams() {
    this.router.navigate(['/teams'])
  }

  goToFields() {
    this.router.navigate(['/fields'])
  }

}
