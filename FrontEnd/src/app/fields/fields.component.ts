import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RestApiService } from "../service/rest-api.service";
import { UserDataService } from "../service/user-data.service";

@Component({
  selector: 'app-fields',
  templateUrl: './fields.component.html',
  styleUrls: ['./fields.component.css']
})
export class FieldsComponent implements OnInit {

  Fields: any = [];

  name: string;
  address: string;
  phone: string;
  capacity: string;
  schedule: string;

  constructor(
    public restApi: RestApiService, 
    public actRoute: ActivatedRoute,
    public router: Router,
    private userData: UserDataService
  ) { }

  ngOnInit() {
    this.loadGameFields()
  }

  // Get fields list
  loadGameFields() {
    return this.restApi.getFields().subscribe((data: {}) => {
      this.Fields = data;
      this.loadDefaultData()
    })
  }

  // Delete field
  deleteGameField(field_id) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.restApi.deleteField(field_id).subscribe(data => {
        this.loadGameFields()
      })
    }
  }  

  viewGameField(field_id) {
    this.name = this.Fields[field_id-1].name
    this.address = this.Fields[field_id-1].address
    this.phone = this.Fields[field_id-1].phone
    this.capacity = this.Fields[field_id-1].capacity
    this.schedule = this.Fields[field_id-1].schedule
  }

  loadDefaultData() {
    this.name = this.Fields[0].name
    this.address = this.Fields[0].address
    this.phone = this.Fields[0].phone
    this.capacity = this.Fields[0].capacity
    this.schedule = this.Fields[0].schedule
  }

  editGameField(field_id) {
    this.router.navigate(['/update-field'])
  }

}
