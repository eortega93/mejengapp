export class Team {
    team_id: string;
    name: string;
    players_quantity: number;
    createdAt: string;
    updatedAt: string;
}
