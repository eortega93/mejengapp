import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {

  private userIdSource = new BehaviorSubject('');
  currentUserId = this.userIdSource.asObservable();

  private nameSource = new BehaviorSubject('');
  currentName = this.nameSource.asObservable();

  constructor() { }

  changeName(name: string) {
    this.nameSource.next(name)
  }

  changeId(id: string) {
    this.userIdSource.next(id)
  }
}
