import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Player } from '../service/player';
import { Field } from '../service/field';
import { Game } from '../service/game';
import { Team } from '../service/team';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { GameTeam } from './game-team';
import { PlayerTeam } from './player-team';

@Injectable({
  providedIn: 'root'
})

export class RestApiService {
  
  // Define API
  apiURL = 'http://localhost:3000';

  constructor(private http: HttpClient) { }

  /*========================================
    CRUD Methods for consuming RESTful API
  =========================================*/

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }  

  /*========================================
    PLAYERS CRUD
  =========================================*/

  // HttpClient API get() method => Fetch players list
  getPlayers(): Observable<Player> {
    return this.http.get<Player>(this.apiURL + '/players')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API get() method => Fetch player
  getPlayer(id): Observable<Player> {
    return this.http.get<Player>(this.apiURL + '/players/' + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  

  // HttpClient API post() method => Create player
  createPlayer(player): Observable<Player> {
    return this.http.post<Player>(this.apiURL + '/players', JSON.stringify(player), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  

  // HttpClient API put() method => Update player
  updatePlayer(id, player): Observable<Player> {
    return this.http.put<Player>(this.apiURL + '/players/' + id, JSON.stringify(player), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API delete() method => Delete player
  deletePlayer(id){
    return this.http.delete<Player>(this.apiURL + '/players/' + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  /*========================================
    FIELDS CRUD
  =========================================*/

  // HttpClient API get() method => Fetch players list
  getFields(): Observable<Field> {
    return this.http.get<Field>(this.apiURL + '/gameFields')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API get() method => Fetch player
  getField(id): Observable<Field> {
    return this.http.get<Field>(this.apiURL + '/gameFields/' + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  

  // HttpClient API post() method => Create player
  createField(field): Observable<Field> {
    return this.http.post<Field>(this.apiURL + '/gameFields', JSON.stringify(field), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  

  // HttpClient API put() method => Update player
  updateField(id, field): Observable<Field> {
    return this.http.put<Field>(this.apiURL + '/gameFields/' + id, JSON.stringify(field), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API delete() method => Delete player
  deleteField(id){
    return this.http.delete<Field>(this.apiURL + '/gameFields/' + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }


  /*========================================
    TEAMS CRUD
  =========================================*/

  // HttpClient API get() method => Fetch teams list
  getTeams(): Observable<Field> {
    return this.http.get<Field>(this.apiURL + '/teams')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API get() method => Fetch team by player
  getTeam(user_id): Observable<Field> {
    return this.http.get<Field>(this.apiURL + '/teams' + user_id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  

  // HttpClient API post() method => Create player
  createTeam(team): Observable<Field> {
    console.log("create team")
    return this.http.post<Field>(this.apiURL + '/teams', JSON.stringify(team), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
    console.log("end create team")
  }  

  // HttpClient API put() method => Update player
  updateTeam(id, field): Observable<Field> {
    return this.http.put<Field>(this.apiURL + '/teams/' + id, JSON.stringify(field), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API delete() method => Delete player
  deleteTeam(id){
    return this.http.delete<Field>(this.apiURL + '/teams/' + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  /*========================================
    GAMES CRUD
  =========================================*/

  // HttpClient API get() method => Fetch players list
  getGames(): Observable<Game> {
    return this.http.get<Game>(this.apiURL + '/games')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API get() method => Fetch player
  getGame(id): Observable<Game> {
    return this.http.get<Game>(this.apiURL + '/games/' + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  

  // HttpClient API post() method => Create player
  createGame(game): Observable<Game> {
    return this.http.post<Game>(this.apiURL + '/games', JSON.stringify(game), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  

  // HttpClient API put() method => Update player
  updateGame(id, game): Observable<Game> {
    return this.http.put<Game>(this.apiURL + '/games/' + id, JSON.stringify(game), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API delete() method => Delete player
  deleteGame(id){
    return this.http.delete<Game>(this.apiURL + '/games/' + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  /*========================================
    GAMETEAMS CRUD
  =========================================*/

  // HttpClient API get() method => Fetch players list
  getGameTeams(): Observable<GameTeam> {
    return this.http.get<GameTeam>(this.apiURL + '/gameteams')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  /*========================================
    PLAYERTEAMS CRUD
  =========================================*/

  // HttpClient API get() method => Fetch players list
  getPlayerTeams(): Observable<PlayerTeam> {
    return this.http.get<PlayerTeam>(this.apiURL + '/playerteams')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  /*========================================
    ERROR HANDLING
  =========================================*/
  
  // Error handling 
  handleError(error) {
     let errorMessage = '';
     if(error.error instanceof ErrorEvent) {
       // Get client-side error
       errorMessage = error.error.message;
     } else {
       // Get server-side error
       errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
     }
     window.alert(errorMessage);
     return throwError(errorMessage);
  }

}