export class Field {
    field_id: string;
    name: string;
    capacity: number;
    address: string;
    schedule: string;
    phone: number;
}
