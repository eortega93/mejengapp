export class FullGame {
    game_id: string;
    my_team: string;
    enemy_team: string;
    field_name: string;
    result: string;
    amount_players: string;
    score1: string;
    score2: string;
}
