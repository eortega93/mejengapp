export class Game {
    game_id: string;
    score1: string;
    score2: string;
    scheduled: string;
    field_id: string;
}
