export class Player {
    user_id: string;
    username: string;
    password: string;
    name: string;
    role: boolean;
    email: string;
    phone: string;
    createdAt: string;
    updatedAt: string;
 }