import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SignUpComponent } from './sign-up/sign-up.component';
import { SignInComponent } from './sign-in/sign-in.component';

import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';

import { TeamsComponent } from './teams/teams.component';
import { CreateTeamComponent } from './create-team/create-team.component';
import { UpdateTeamComponent } from './update-team/update-team.component';

import { FieldsComponent} from './fields/fields.component';
import { CreateFieldComponent } from './create-field/create-field.component';
import { UpdateFieldComponent } from './update-field/update-field.component';

import { GamesComponent} from './games/games.component';
import { CreateGameComponent} from './create-game/create-game.component';
import { UpdateGamesComponent} from './update-games/update-games.component';

import { UserManualComponent} from './user-manual/user-manual.component';


const routes: Routes = [
  { path: 'sign-in', component: SignInComponent },
  { path: '', pathMatch: 'full', redirectTo: 'sign-in' },
  { path: 'sign-up', component: SignUpComponent },
  
  { path: 'dashboard', component: DashboardComponent },
  { path: 'admin-dashboard', component: AdminDashboardComponent },

  { path: 'teams', component: TeamsComponent },
  { path: 'create-team', component: CreateTeamComponent },
  { path: 'update-team', component: UpdateTeamComponent },

  { path: 'fields', component: FieldsComponent},
  { path: 'create-field', component: CreateFieldComponent },
  { path: 'update-field/:field_id', component: UpdateFieldComponent},
  
  { path: 'games', component: GamesComponent},
  { path: 'create-game', component: CreateGameComponent },
  { path: 'update-games/:game_id', component: UpdateGamesComponent},

  { path: 'user-manual', component: UserManualComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }