import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserDataService } from "../service/user-data.service";
import { RestApiService } from "../service/rest-api.service";
import { FullGame } from '../service/full-game';
import { stringify } from '@angular/compiler/src/util';
import { log } from 'util';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})
export class GamesComponent implements OnInit {

  Teams: any = [];
  Games: any = [];
  GameTeams: any = [];
  PlayerTeams: any = [];
  Fields: any = [];
  FinalGames: any = [];
  CurrentGame = new FullGame();
  user_id:string;

  constructor(
    public restApi: RestApiService, 
    public actRoute: ActivatedRoute,
    public router: Router,
    private userData: UserDataService
  ) { }

  ngOnInit() {
    this.userData.currentUserId.subscribe(user_id => this.user_id = user_id)
    this.loadGames();
  }

  loadGames() {
    return this.restApi.getFields().subscribe((data: {}) => {
      this.Fields = data;
      return this.restApi.getTeams().subscribe((data: {}) => {
        this.Teams = data;
        return this.restApi.getGames().subscribe((data: {}) => {
          this.Games = data;
          return this.restApi.getGameTeams().subscribe((data: {}) => {
            this.GameTeams = data;
            return this.restApi.getPlayerTeams().subscribe((data: {}) => {
              this.PlayerTeams = data;
              this.createFinalGames();
              this.loadCurrentGame();
            })
          })
        })
      })
    })
  }

  createFinalGames() {
    this.PlayerTeams.forEach(playerTeam => {
      if (playerTeam.player_id == this.user_id) {
        this.Games.forEach(game => {
          this.GameTeams.forEach(gameTeam => {
            if (game.game_id == gameTeam.game_id && gameTeam.team_id != playerTeam.team_id) {
              var fullGame = new FullGame();
              fullGame.game_id = game.game_id;
              fullGame.score1 = game.score1;
              fullGame.score2 = game.score2;
              if (+game.score1 < +game.score2) {
                fullGame.result = "Derrota";
              } else if (+game.score1 > +game.score2) {
                fullGame.result = "Victoria";
              } else {
                fullGame.result = "Empate";
              }
              this.Fields.forEach(field => {
                if (game.field_id == field.field_id) {
                  fullGame.field_name = field.name
                  fullGame.amount_players = field.capacity
                }
              });
              this.Teams.forEach(team => {
                if (team.team_id == gameTeam.team_id) {
                  fullGame.enemy_team = team.name;
                }
                if (team.team_id == playerTeam.team_id) {
                  fullGame.my_team = team.name;
                }
              });
              this.FinalGames.push(fullGame)
            }
          });
        });
      }
    });
  }

  loadCurrentGame() {
    this.CurrentGame.game_id = this.FinalGames[0].game_id
    this.CurrentGame.my_team = this.FinalGames[0].my_team
    this.CurrentGame.enemy_team = this.FinalGames[0].enemy_team
    this.CurrentGame.field_name = this.FinalGames[0].field_name
    this.CurrentGame.amount_players = this.FinalGames[0].amount_players
    this.CurrentGame.result = this.FinalGames[0].result
    this.CurrentGame.score1 = this.FinalGames[0].score1
    this.CurrentGame.score2 = this.FinalGames[0].score2
  }

  viewGame(game_id) {
    this.CurrentGame.game_id = this.FinalGames[game_id-1].game_id
    this.CurrentGame.my_team = this.FinalGames[game_id-1].my_team
    this.CurrentGame.enemy_team = this.FinalGames[game_id-1].enemy_team
    this.CurrentGame.field_name = this.FinalGames[game_id-1].field_name
    this.CurrentGame.amount_players = this.FinalGames[game_id-1].amount_players
    this.CurrentGame.result = this.FinalGames[game_id-1].result
    this.CurrentGame.score1 = this.FinalGames[game_id-1].score1
    this.CurrentGame.score2 = this.FinalGames[game_id-1].score2
  }
}
