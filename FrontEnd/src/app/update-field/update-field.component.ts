import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from "../service/rest-api.service";
import { UserDataService } from "../service/user-data.service";

@Component({
  selector: 'app-update-field',
  templateUrl: './update-field.component.html',
  styleUrls: ['./update-field.component.css']
})
export class UpdateFieldComponent implements OnInit {

  field_id = this.actRoute.snapshot.params['field_id'];
  fieldData: any = {};

  constructor(
    public restApi: RestApiService, 
    public actRoute: ActivatedRoute,
    public router: Router,
    private userData: UserDataService
  ) { }

  ngOnInit() {
    this.restApi.getField(this.field_id).subscribe((data: {}) => {
      this.fieldData = data;
      console.log(this.field_id);
      console.log(this.fieldData);
    })
  }

   updateGameField() {
    if(window.confirm('Are you sure, you want to update?')){
      this.restApi.updateField(this.fieldData.field_id, this.fieldData).subscribe(data => {
        this.router.navigate(['/fields'])
      })
    }
  }

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

}
