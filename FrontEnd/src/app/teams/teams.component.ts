import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from "../service/rest-api.service";
import { UserDataService } from "../service/user-data.service";
import { Team } from '../service/team';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css']
})
export class TeamsComponent implements OnInit {

  @Input() teamDetails = { team_id: '', name: '', players_quantity: 0}
  Teams: any = [];
  errorMessage = '';
  team: Team = new Team;

  constructor(
    public restApi: RestApiService, 
    public router: Router,
    private userData: UserDataService
  ) { }

  ngOnInit() {
    this.restApi.getTeams().subscribe(
      (data: {}) => {
        this.Teams = data;   
      }
    )
  }

  registerTeams(){
    if(this.checkTeamData()){
      this.team.name = this.teamDetails.name;
      this.team.players_quantity = 0;
      this.restApi.createTeam(this.team)
      this.router.navigate(['/teams'])
    }
  }

  checkTeamData(){
    if(this.teamDetails.name === ""){
      this.errorMessage = "Empty name field"
      return false
    }
    return true
  }

}