import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreateTeamComponent } from './create-team/create-team.component';
import { UpdateTeamComponent } from './update-team/update-team.component';

import { CreateFieldComponent } from './create-field/create-field.component';

import { TeamsComponent } from './teams/teams.component';
import { GamesComponent } from './games/games.component';
import { UpdateGamesComponent } from './update-games/update-games.component';
import { FieldsComponent } from './fields/fields.component';
import { CreateGameComponent } from './create-game/create-game.component';
import { UpdateFieldComponent } from './update-field/update-field.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { UserManualComponent } from './user-manual/user-manual.component';


@NgModule({
  declarations: [
    AppComponent,
    CreateTeamComponent,
    UpdateTeamComponent,
    CreateFieldComponent,
    TeamsComponent,
    GamesComponent,
    UpdateGamesComponent,
    FieldsComponent,
    CreateGameComponent,
    UpdateFieldComponent,
    SignUpComponent,
    SignInComponent,
    DashboardComponent,
    AdminDashboardComponent,
    UserManualComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
