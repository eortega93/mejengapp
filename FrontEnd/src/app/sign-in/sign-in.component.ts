import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from "../service/rest-api.service";
import { UserDataService } from "../service/user-data.service";

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  @Input() playerDetails = { email: '', password: '' }
  Players: any = [];
  errorMessage = '';
  loginSuccess = true;
  checkResult = false;

  username = "";
  user_id = "";

  constructor(
    public restApi: RestApiService, 
    public router: Router,
    private userData: UserDataService
  ) { }

  ngOnInit() {}

  login() {
    this.errorMessage = '';

    this.restApi.getPlayers().subscribe(
      (data: {}) => {
        this.Players = data;   
        this.loginSuccess = this.checkCredentials();
        if (this.loginSuccess) {
          this.newName(this.username)
          this.newId(this.user_id)
          this.router.navigate(['/dashboard'])
        }
        else {
          console.log("Login was unsuccessful");
        }
        },
      (error) => {
        this.errorMessage = error.message; 
        }
    )
  }

  checkCredentials() {
    this.checkResult = false;

    this.Players.forEach(element => {
      if(element.email === this.playerDetails.email && element.password === this.playerDetails.password) {
        this.checkResult = true;
        this.username = element.name;
        this.user_id = element.user_id;
      }   
    });
    
    return this.checkResult;
  }

  newName(content) {
    this.userData.changeName(content);
  }

  newId(content) {
    this.userData.changeId(content);
  }
}
