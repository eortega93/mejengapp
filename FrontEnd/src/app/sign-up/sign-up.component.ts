import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from "../service/rest-api.service";
import { UserDataService } from "../service/user-data.service";
import { Player } from '../service/player';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  @Input() playerDetails = { name: '', username: '', email: '', phone: '', password: '', passwordConfirm: '' }
  Players: any = [];
  errorMessage = '';
  signInSuccess = true;
  //checkResult = false;
  player: Player = new Player;
  //username = "";
  //user_id = "";

  constructor(
    public restApi: RestApiService, 
    public router: Router,
    private userData: UserDataService
  ) { }

  ngOnInit() {}

  registerUser() {
    this.errorMessage = '';

    this.restApi.getPlayers().subscribe(
      (data: {}) => {
        this.Players = data;   
        if (this.checkUserData()) {
          this.player.username = this.playerDetails.username;
          this.player.password = this.playerDetails.password;
          this.player.name = this.playerDetails.name;
          this.player.role = false;
          this.player.email = this.playerDetails.email;
          this.player.phone = this.playerDetails.phone;
          let date = new Date();
          this.player.createdAt = "2019-10-10T06:00:00.000Z";
          this.player.updatedAt = "2019-10-10T06:00:00.000Z";

          console.log("\nTODO BIEN\n")
          this.restApi.createPlayer(this.player)
          console.log("\nPLayer creado BIEN\n")
          this.newName(this.playerDetails.name)
          //this.newId(this.playerDetails.user_id)
          this.router.navigate(['/dashboard'])
        }
        else {
          console.log("Login was unsuccessful");
          console.log("errorMessage:" + this.errorMessage)
        }
        },
      (error) => {
        this.errorMessage = error.message; 
        }
    )
  }

  checkUserData(){
    if(this.playerDetails.name === ""){
      this.errorMessage = "Empty name field"
      return false
    }
    if(this.playerDetails.username === ""){
      this.errorMessage = "Empty username field"
      return false
    }
    if(this.playerDetails.email === ""){
      this.errorMessage = "Empty email field"
      return false
    }
    if(this.playerDetails.password === ""){
      this.errorMessage = "Empty password field"
      return false
    }
    if(this.playerDetails.passwordConfirm === ""){
      this.errorMessage = "Empty confirm password field"
      return false
    }
    if(this.playerDetails.phone === ""){
      this.errorMessage = "Empty phone field"
      return false
    }
    if(this.playerDetails.password !== this.playerDetails.passwordConfirm){
      this.errorMessage = "Entered password doesnt match"
      return false
    }

    this.Players.forEach(element => {
      if(element.username === this.playerDetails.username){
        this.errorMessage = "Entered username already exists"
        return false
      }  
      if(element.email === this.playerDetails.email){
        this.errorMessage = "Entered email already exists"
        return false
      }
      if(element.phone === this.playerDetails.phone){
        this.errorMessage = "Entered phone already exists"
        return false
      }      
    });
    
    return true
  }

  newName(content) {
    this.userData.changeName(content);
  }

  newId(content) {
    this.userData.changeId(content);
  }
}
