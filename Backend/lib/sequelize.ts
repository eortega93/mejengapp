import {Sequelize} from 'sequelize-typescript';
//import {Op} from 'sequelize';

export const sequelize = new Sequelize({
  database: 'mejengapp',
  dialect: 'postgres',
  username: 'postgres',
  password: 'jose12345',
  host: 'localhost',
  port: 5432,
  //operatorAliases: Op,
  models: [__dirname + '/models']
 });
 