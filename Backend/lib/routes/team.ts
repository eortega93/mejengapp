import {Router} from 'express';
import {Team} from '../models/Team';

export const team = Router();

team.post('/', async (req, res, next) => {
  try {
    const team = await Team.create(req.body);
    res.status(201).json(team);
  } catch (e) {
    next(e);
  }
});

team.get('', async (req, res, next) => {
    console.log('\nteam get\n');
  try {
    res.json(await Team.scope(req.query['scope']).findAll());
  } catch (e) {
    next(e);
  }
});

team.get('/:team_id', async (req, res, next) => {
  try {
    const team = await Team.scope(req.query['scope']).findByPk(req.params['team_id']);
    res.json(team);
  } catch (e) {
    next(e);
  }
});

team.put('/:team_id', async (req, res, next) => {
  try {
    await Team.update<Team>(req.body, {where: {id: req.params['team_id']}});
    res.sendStatus(200);
  } catch (e) {
    next(e);
  }
});
