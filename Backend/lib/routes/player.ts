import {Router} from 'express';
import {Player} from '../models/Player';
import { PlayerTeam } from '../models/PlayerTeam';

export const player = Router();

player.post('', async (req, res, next) => {
  try {
    console.log("\nCREAR PLAYER\n")
    console.log(JSON.stringify(req.body))
    const player = await Player.create(req.body);
    res.status(201).json(player);
    console.log("post:player/ " + player);
  } catch (e) {
    next(e);
  }
});

player.put('', async (req, res, next) => {
  try {
    console.log("\nCREAR PLAYER\n")
    console.log(JSON.stringify(req.body))
    const player = await Player.create(req.body);
    res.status(201).json(player);
    console.log("post:player/ " + player);
  } catch (e) {
    next(e);
  }
});

player.get('', async (req, res, next) => {
  try {
    
    const players = await Player.scope(req.query['scope']).findAll();
    res.json(players);
    console.log("123get:player/ " + players);
  } catch (e) {
    next(e);
  }
});

player.get('/:user_id', async (req, res, next) => {
  try {
    const player = await Player.scope(req.query['scope']).findByPk(req.params['user_id']);
    res.json(player);
    console.log("get:player/"+ req.params['user_id'] +": "+ player);
  } catch (e) {
    next(e);
  }
});

player.put('/:user_id', async (req, res, next) => {
  try {
    await Player.update<Player>(req.body, {where: {id: req.params['user_id']}});
    res.sendStatus(200);
    console.log("put:player/"+ req.params['user_id']);
  } catch (e) {
    next(e);
  }
});

player.post('/:user_id/teams/:team_id', async (req, res, next) => {
  try {
    await PlayerTeam.create({
      user_id: req.params['user_id'], team_id: req.params['team_id']
    });
    console.log("post:player/"+ req.params['user_id']);
    res.sendStatus(200);
  } catch (e) {
    next(e);
  }
});

