import {Router} from 'express';
import {Game} from '../models/Game';
import {GameTeams} from '../models/GameTeams';

export const game = Router();

game.post('/', async (req, res, next) => {
  try {
    const game = await Game.create(req.body);
    res.status(201).json(game);
  } catch (e) {
    next(e);
  }
});

game.post('/:game_id/Team/:team_id', async (req, res, next) => {
  try {
    await GameTeams.create({
        game_id: req.params['game_id'], team_id: req.params['team_id']
    });
    res.sendStatus(200);
  } catch (e) {
    next(e);
  }
});

game.get('', async (req, res, next) => {
  try {
    res.json(await Game.scope(req.query['scope']).findAll());
  } catch (e) {
    next(e);
  }
});

game.get('/:game_id', async (req, res, next) => {
  try {
    const actor = await Game.scope(req.query['scope']).findByPk(req.params['game_id']);
    res.json(actor);
  } catch (e) {
    next(e);
  }
});

game.put('/:game_id', async (req, res, next) => {
  try {
    await Game.update(req.body, {where: {game_id: req.params['game_id']}});
    res.sendStatus(200);
  } catch (e) {
    next(e);
  }
});
