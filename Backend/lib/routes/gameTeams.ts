import {Router} from 'express';
import {GameTeams} from '../models/GameTeams';

export const gameTeams = Router();

gameTeams.get('', async (req, res, next) => {
  try {
    res.json(await GameTeams.scope(req.query['scope']).findAll());
  } catch (e) {
    next(e);
  }
});
