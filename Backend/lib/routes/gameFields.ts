import {Router} from 'express';
import {GameField} from '../models/GameField';
import {Game} from '../models/Game';

export const gameFields = Router();

gameFields.post('/', async (req, res, next) => {
  try {
    const gameField = await GameField.create(req.body);
    res.status(201).json(gameField);
  } catch (e) {
    next(e);
  }
});

gameFields.post('/:field_id/Games/:game_id', async (req, res, next) => {
  try {
    await Game.create({
        field_id: req.params['field_id'], game_id: req.params['game_id']
    });
    res.sendStatus(200);
  } catch (e) {
    next(e);
  }
});

gameFields.get('', async (req, res, next) => {
  try {
    res.json(await GameField.scope(req.query['scope']).findAll());
  } catch (e) {
    next(e);
  }
});

gameFields.get('/:field_id', async (req, res, next) => {
  try {
    const actor = await GameField.scope(req.query['scope']).findByPk(req.params['field_id']);
    res.json(actor);
  } catch (e) {
    next(e);
  }
});

gameFields.put('/:field_id', async (req, res, next) => {
  try {
    await GameField.update(req.body, {where: {field_id: req.params['field_id']}});
    res.sendStatus(200);
  } catch (e) {
    next(e);
  }
});
