import {Router} from 'express';
import {PlayerTeam} from '../models/PlayerTeam';

export const playerTeams = Router();

playerTeams.get('', async (req, res, next) => {
  try {
    res.json(await PlayerTeam.scope(req.query['scope']).findAll());
  } catch (e) {
    next(e);
  }
});
