import {Model, AutoIncrement, Column, Table, ForeignKey, PrimaryKey, Scopes, BelongsToMany, BelongsTo} from "sequelize-typescript";
import {GameField} from "./GameField";
import {GameTeams} from "./GameTeams";
import { Team } from "./Team";

@Scopes(() => ({
  gameFields: {
    include: [
      {
        model: GameField,
        through: {attributes: []},
      },
    ],
  },
  teams: {
    include: [{
      model: Team,
      through: {attributes: []},
    }],
  }
}))
@Table
export class Game extends Model<Game> {

  @PrimaryKey
  @AutoIncrement
  @Column
  game_id!: number;

  @Column
  score1!: string;

  @Column
  score2!: string;

  @Column
  scheduled!: string;

  @ForeignKey(() => GameField)
  @Column
  field_id?: number;

  @BelongsTo(() => GameField)
  gameFields?: GameField;

  @BelongsToMany(() => Team, () => GameTeams)
  teams?: Team[];
}

//un post tiene 1 autor pero un autor muchos post

//un game tiene 1 field pero un field  tiene muchos games