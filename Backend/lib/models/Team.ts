import {Model, AutoIncrement, Column, Table, PrimaryKey, Scopes, BelongsToMany} from "sequelize-typescript";
import {Player} from "./Player";
import {PlayerTeam} from "./PlayerTeam";

@Scopes(() => ({
  players: {
    include: [{
      model: Player,
      through: {attributes: []},
    }],
  }
}))
@Table
export class Team extends Model<Team> {

  @PrimaryKey
  @AutoIncrement
  @Column
  team_id!: number;

  @Column
  name!: string;

  @Column
  players_quantity!: number;

  @BelongsToMany(() => Player, () => PlayerTeam)
  players?: Player[];
}
