import {Model, Column, AutoIncrement, Table, ForeignKey, PrimaryKey} from "sequelize-typescript";
import {Player} from "./Player";
import {Team} from "./Team";

@Table
export class PlayerTeam extends Model<PlayerTeam> {

  @PrimaryKey
  @AutoIncrement
  @Column
  player_team_pkey!: number;

  @ForeignKey(() => Player)
  @Column
  player_id!: number;

  @ForeignKey(() => Team)
  @Column
  team_id!: number;
}
