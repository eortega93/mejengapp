import {Model, AutoIncrement , Column, Table, PrimaryKey, Scopes, BelongsToMany} from "sequelize-typescript";
import {Team} from "./Team";
import {PlayerTeam} from "./PlayerTeam";

@Scopes(() => ({
  teams: {
    include: [
      {
        model: Team,
        through: {attributes: []},
      },
    ],
  },
}))
@Table
export class Player extends Model<Player> {

  @PrimaryKey
  @AutoIncrement
  @Column
  user_id!: number;

  @Column
  username!: string;

  @Column
  password!: string;

  @Column
  name!: string;

  @Column
  role!: boolean;

  @Column
  email!: string;

  @Column
  phone!: string;

  @BelongsToMany(() => Team, () => PlayerTeam)
  teams?: Team[];
}
