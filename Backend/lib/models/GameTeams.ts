import {Model, Column, AutoIncrement, Table, ForeignKey, PrimaryKey} from "sequelize-typescript";
import {Game} from "./Game";
import {Team} from "./Team";

@Table
export class GameTeams extends Model<GameTeams> {

  @PrimaryKey
  @AutoIncrement
  @Column
  game_teams_pkey!: number;

  @ForeignKey(() => Game)
  @Column
  game_id!: number;

  @ForeignKey(() => Team)
  @Column
  team_id!: number;
}
