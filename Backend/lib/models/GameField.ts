import {Model, AutoIncrement, Column, Table, PrimaryKey} from "sequelize-typescript";

@Table
export class GameField extends Model<GameField> {

  @PrimaryKey
  @AutoIncrement
  @Column
  field_id!: number;

  @Column
  name!: string;

  @Column
  capacity!: number;

  @Column
  address!: string;

  @Column
  services!: string;

  @Column
  schedule!: string;

  @Column
  phone!: number;
}