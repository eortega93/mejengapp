import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as errorhandler from 'strong-error-handler';
import {player} from './routes/player';
import {team} from './routes/team';
import { gameFields } from './routes/gameFields';
import { game } from './routes/games';
import { gameTeams } from './routes/gameTeams';
import { playerTeams } from './routes/playerTeams';
export const app = express();

// middleware for parsing application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: true}));

// middleware for json body parsing
app.use(bodyParser.json({limit: '5mb'}));

// enable corse for all origins
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Expose-Headers", "x-total-count");
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,PATCH");
  res.header("Access-Control-Allow-Headers", "Content-Type,authorization");

  next();
});

app.use('/players', player);
app.use('/teams', team);
app.use('/games', game);
app.use('/gameFields', gameFields);
app.use('/gameTeams', gameTeams);
app.use('/playerTeams', playerTeams);


app.use(errorhandler({
  debug: process.env.ENV !== 'prod',
  log: true,
}));
